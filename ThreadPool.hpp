#pragma once 

#include <vector>
#include <iostream>
#include <string>
#include <thread>
#include <functional>
#include "ThreadSafeQueue.hpp"

/**************************************************************
*  My own implementation of the thread pool, uses funcional  *
**************************************************************/

class ThreadPool {
 	 private:
		 void workerMain();

		 std::atomic_bool running;
		 std::mutex l;
		 std::vector<std::thread> workers;

		 ThreadSafeQueue<std::function<void()> > tasks;

   public:
		 //Constructor and destructor
		 ThreadPool();
		 ~ThreadPool();

		 //Non-copyable
		 ThreadPool (const ThreadPool&) = delete;

		 //Non-assignable
		 ThreadPool& operator=(const ThreadPool&) = delete;

		 //Functions
		 template <typename Func, typename... Args>
		 void addToExecution(Func&& f, Args&&... args) {
		 	//This binds the function pointer to the arguments provided, creating a 
		 	//functor, with all arguments already passed to it
		 	auto task = std::bind(std::forward<Func>(f), std::forward<Args>(args)...);
		 	tasks.enqueue(task);
		 }

		 void destroy(); //Invalidates the queue and joins threads

};

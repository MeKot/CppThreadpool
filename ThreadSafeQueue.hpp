#pragma once 

#include <atomic>
#include <queue>
#include <mutex>
#include <utility>

template <typename T>
class ThreadSafeQueue {

	private:
		std::queue<T> queue;
		std::atomic_bool valid{true};
		mutable std::mutex m; 
		std::condition_variable condition;

	public:
		//Will use the default constructor, since no need to create new objects
		//Destructor
    ~ThreadSafeQueue() {
    	invalidate();
    }

		//An attempt to dequeue an item, returns false if unsuccessful
		bool tryDequeue(T& out) {
			std::lock_guard<std::mutex> lock{m};
			if (queue.empty() || !valid) {
		 	  return false; 
			}
			out = std::move(queue.front());
			queue.pop();
			return true;
		}

		//Waiting to dequeue an item from the queue, feturns true upon success
		bool waitDequeue(T& out) {
		 	 std::unique_lock<std::mutex> lock{m};
			 //A predicate that is satisfied by the empty or invalid queue
			 condition.wait(lock, [this](){
					  	 return !queue.empty() || !valid;
					 }); //This makes sure that the queue is never empty here
			 if (!valid) {
		 	   return false;
			 }
			 out = std::move(queue.front());
			 queue.pop();
			 return true;
		}

		//Pushing an item in the queue
		void enqueue(T value) {
		 	 std::lock_guard<std::mutex> lock{m};
			 queue.push(value);
			 condition.notify_one(); //Notifies one waiting thread
		}

		//Clearing the queue (deleting all items)
		void clear() {
		 	 std::lock_guard<std::mutex> lock{m};
			 queue.swap(std::queue<T>{});
			 condition.notify_all();
		}

		//Checking that the queue is empty
		bool isEmpty() {
			std::lock_guard<std::mutex> lock{m};
			return queue.empty();
		}

		// A Method that invalidates the queue, so that any firther use will throw an error
		void invalidate() {
		 	 std::lock_guard<std::mutex> lock{m};
			 valid = false;
			 condition.notify_all();
		}

		//Ensuring that no midifications of members is possible inside
		bool isValid() const {
			std::lock_guard<std::mutex> lock{m};
			return valid;
		}

};








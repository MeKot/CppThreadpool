#include "ThreadPool.hpp"

ThreadPool::ThreadPool() :running(true) {
	//Since the creator thread is also running...
 	int numberOfWorkers = std::thread::hardware_concurrency() - 1;
	//Making sure that at least one thread is spawned.
	if (numberOfWorkers == 0) {
 	 numberOfWorkers = 1;
	}
	workers.reserve(numberOfWorkers);
	for (int i = 0; i < numberOfWorkers; ++i) {
		workers.emplace(workers.end(), &ThreadPool::workerMain, this);
	}
}

ThreadPool::~ThreadPool() {
	 //Making sure that all threads finish execution
	 destroy();
}

void ThreadPool::workerMain() {
 	 while (running) {
 			std::function<void()> func{nullptr};
			if (tasks.waitDequeue(func)) {
 				func();
			}
	 }
}


void ThreadPool::destroy() {
 	 running = false;
	 tasks.invalidate();
	 for (auto& worker : workers) {
		 if (worker.joinable()) {
 	 	   worker.join(); //joins all running threads 
		 }
	 }
	 workers.clear();
}
